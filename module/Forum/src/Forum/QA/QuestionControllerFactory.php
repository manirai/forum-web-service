<?php
/**
 * Created by PhpStorm.
 * User: mani
 * Date: 12/11/15
 * Time: 10:53 PM
 */

namespace Forum\QA;


use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class QuestionControllerFactory implements FactoryInterface {

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator) {

        $coreServiceLocator = $serviceLocator->getServiceLocator();
        $questionService = $coreServiceLocator->get('questionService');

        return new QuestionController($questionService);
    }
}
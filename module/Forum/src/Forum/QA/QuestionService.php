<?php
/**
 * Created by PhpStorm.
 * User: mani
 * Date: 12/11/15
 * Time: 10:42 PM
 */

namespace Forum\QA;


interface QuestionService
{

    public function createQuestion(Question $question);

    public function getQuestion($id);

    public function getQuestions();
}
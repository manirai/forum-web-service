<?php
/**
 * Created by PhpStorm.
 * User: mani
 * Date: 12/11/15
 * Time: 10:35 PM
 */

namespace Forum\QA;


class QuestionBuilder {

    public static function buildQuestion($data) {
        $question = new Question();
        $question->setTitle($data["title"]);
        $question->setDetail($data["detail"]);
        return $question;
    }
}
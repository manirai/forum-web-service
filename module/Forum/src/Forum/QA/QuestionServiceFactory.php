<?php
/**
 * Created by PhpStorm.
 * User: mani
 * Date: 12/12/15
 * Time: 12:43 AM
 */

namespace Forum\QA;


use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class QuestionServiceFactory implements FactoryInterface {

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator) {
        $entityManager = $serviceLocator->get("Doctrine\ORM\EntityManager");
        return new QuestionServiceImpl($entityManager);
    }
}
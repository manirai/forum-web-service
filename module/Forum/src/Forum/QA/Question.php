<?php
/**
 * Created by PhpStorm.
 * User: mani
 * Date: 12/11/15
 * Time: 12:02 AM
 */

namespace Forum\QA;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\GeneratedValue;

/**
 * @Entity
 * @Table(name = "question")
 */
class Question
{

    /**
     * @Id
     * @GeneratedValue(strategy="AUTO")
     * @Column(name = "id", type = "bigint")"
     */
    private $id;

    /**
     * @Column(name = "title", type = "string", length = "150", nullable = "false")
     */
    private $title;

    /**
     * @Column(name = "detail", type = "string", length = "30000", nullable = "false")
     */
    private $detail;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getDetail()
    {
        return $this->detail;
    }

    public function setDetail($detail)
    {
        $this->detail = $detail;
    }

    public function toArray() {

        return [
          "id" => $this->id,
            "title" => $this->title,
            "detail" => $this->detail
        ];
    }
}
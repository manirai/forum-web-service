<?php
/**
 * Created by PhpStorm.
 * User: mani
 * Date: 12/11/15
 * Time: 10:44 PM
 */

namespace Forum\QA;


use Doctrine\ORM\EntityManager;

class QuestionServiceImpl implements QuestionService {

    private $entityManager;

    public function __construct(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }

    public function createQuestion(Question $question) {
        $this->entityManager->persist($question);
        $this->entityManager->flush();
    }

    public function getQuestion($id) {
        return $this->entityManager->find("Forum\QA\Question", $id);
    }

    public function getQuestions() {
        return $this->entityManager->find("Forum\QA\Question");
    }
}
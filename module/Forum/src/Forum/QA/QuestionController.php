<?php
/**
 * Created by PhpStorm.
 * User: mani
 * Date: 12/11/15
 * Time: 12:00 AM
 */

namespace Forum\QA;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class QuestionController extends AbstractRestfulController {

    private $questionService;

    public function __construct(QuestionService $questionService) {
        $this->questionService = $questionService;
    }

    public function create($data) {
        $question = QuestionBuilder::buildQuestion($data);
        $this->questionService->createQuestion($question);
        return new JsonModel($question->toArray());
    }

    public function get($id) {

        $question = $this->questionService->getQuestion($id);
        return new JsonModel($question->toArray());
    }

    public function getList() {
        $questions = $this->questionService->getQuestions();
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: mani
 * Date: 6/2/15
 * Time: 12:44 AM
 */

namespace Forum;


class Module {

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
} 
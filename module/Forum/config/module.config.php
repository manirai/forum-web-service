<?php
/**
 * Created by PhpStorm.
 * User: mani
 * Date: 6/1/15
 * Time: 10:40 PM
 */
return array(
    // Configuring controllers
    'controllers' => array(
        // Using factories because we needed to customize controller initialization
        'factories' => array(
            'questionController' => 'Forum\QA\QuestionControllerFactory'
        ),
    ),

    'router' => array(
        'routes' => array(
            'question' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/api/v1/questions[/:id]',
                    'defaults' => array(
                        'controller' => 'questionController'
                    )
                )
            )
        ),
    ),

    'doctrine' => array(
        'driver' => array(
            'application_entities' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/Forum')
            ),
            'orm_default' => array(
                'drivers' => array(
                    'Forum' => 'application_entities'
                )
            )
        )
    ),

    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy'
        ),
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5'
    ),

    'service_manager' => array(
        'factories' => array(
            'questionService' => 'Forum\QA\QuestionServiceFactory'
        )
    )
);
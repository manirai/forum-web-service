<?php
/**
 * Created by PhpStorm.
 * User: Aron
 * Date: 07-Jun-15
 * Time: 12:15 AM
 */

return array(
    'doctrine' => array(
        'connection' => array(
            'orm_default' => array(
                'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
                'params' => array(
                    'host' => 'localhost',
                    'port' => '3306',
                    'user' => 'root',
                    'password' => 'rai.mani',
                    'dbname' => 'forum',
                )
            )
        )
    )
);